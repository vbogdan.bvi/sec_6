/**
 * @author            : Viktor Bogdan
 * @last modified on  : 07-02-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-28-2021   Viktor Bogdan   Initial Version
 **/
public with sharing class RickAndMortyHandler {
  public RickAndMortyHandler() {
  }

  /*
    1. Создайте апекс триггер на объекте Contact . Логика его работы должна быть такова, что при
    создании нового контакта с именем Rick и фамилией Sanchez, автоматически должен
    создаваться еще и Морти (новый контакт с именем Morty и фамилийе Smith ).
    */

  public static void addMortyToEveryRick(List<Contact> contacts) {
    List<Contact> motryList = new List<Contact>();
    for (Contact con : contacts) {
      if (con.FirstName == 'Rick' && con.LastName == 'Sanchez') {
        // Contact mortyCon = new Contact(
        //   FirstName = 'Morty',
        //   LastName = 'Smith',
        // );
        Contact mortyCon = new Contact(
          FirstName = 'Morty',
          LastName = 'Smith',
          MyRick__c = con.ID,
          SadMorty__c = false
        );
        motryList.add(mortyCon);
      }
    }
    insert motryList;
  }

  /*
    2. Обновите свой триггер так, чтобы когда для нового контакта с именем Rick и фамилией
    Sanchez, автоматически создается Морти (новый контакт с именем Morty и фамилийе Smith ),
    у Морти поле MyRick заполняется значением Id Rick'а для которого он создан.

    Кроме того, обновите свой триггер так, чтобы когда создается или обновляется контакт с
    именем Morty и фамилией Smith, проверяется заполнено ли поле MyRick. Если MyRick пустое,
    то значение checkbox SadMorty должно стать true, если MyRick не пустое, то значение
    SadMorty = false.
    */
  public static void checkSadMotry(List<Contact> contacts) {
    for (Contact cont : contacts) {
      if (cont.FirstName == 'Morty' && cont.LastName == 'Smith') {
        if (cont.MyRick__c == null) {
          cont.SadMorty__c = true;
          //   System.debug('inside checkSadMotry if');
        } else {
          cont.SadMorty__c = false;
          //   System.debug('inside checkSadMotry else');
        }
      }
    }
  }

  /*
  
   3. Обновите свой триггер так, чтобы при создании Рика, ему больше автоматически не создавался
    Морти.
    Логика SadMorty чекбокса должна остаться неизменной.
    Обновите свой триггер так, чтобы при инсерте в базу данных списка контактов, каждому
    Морти в этом списке был приписан один Рик (в поле MyRick ) из этого же списка. Если Морти
    не хватило Рика - он остается без Рика.
  */

  public static void addRickToMorty(List<COntact> contacts) {
    List<Contact> rickList = [
      SELECT id, FirstName, LastName
      FROM Contact
      WHERE id IN :contacts AND FirstName = 'Rick' AND LastName = 'Sanchez'
    ];

    List<Contact> mortyList = [
      SELECT id, FirstName, LastName, SadMorty__c, MyRick__c
      FROM Contact
      WHERE id IN :contacts AND FirstName = 'Morty' AND LastName = 'Smith'
    ];

    System.debug('rickList.size = ' + rickList.size());
    System.debug('mortyList.size = ' + mortyList.size());

    Integer i = 0;
    for (COntact morty : mortyList) {
      if (i < rickList.size()) {
        morty.MyRick__c = rickList.get(i).ID;
        System.debug(' inside fori if   ' + i + '   ' + '-----------------' + rickList.get(i).ID);
        i++;
      } else {
        break;
      }
    }
    update mortyList;
  }

  /*
  4. Обновите свой триггер так, чтобы при создании нового Морти, ему приписывался свободный
  Рик (из создаваемых сейчас или уже ранее созданных), т.е. Рик, который не приписан ни к
  одному Морти.
  
  */
  public static void addFreeRickToMorty(List<Contact> contacts) {
    // список всех Риков
    list<Contact> freeRickList = [
      SELECT FirstName, Id, LastName, (SELECT Id FROM MyRicks__r)
      FROM Contact
      WHERE FirstName = 'Rick' AND LastName = 'Sanchez'
    ];
    // убираем из списка Риков всех, кто уже привязан к Морти(остаються свободные Рики)
    for (Integer i = freeRickList.size() - 1; i >= 0; i--) {
      if (!freeRickList.get(i).MyRicks__r.isEmpty()) {
        freeRickList.remove(i);
      }
    }
    // список новых Морти
    List<Contact> newMortyList = [
      SELECT id, FirstName, LastName, MyRick__c
      FROM Contact
      WHERE id IN :contacts AND FirstName = 'Morty' AND LastName = 'Smith'
    ];
    // привязываем к свободному Рику Морти
    integer j = 0;
    for (Contact cont : newMortyList) {
      if (cont.MyRick__c == null) {
        if (j < freeRickList.size()) {
          cont.MyRick__c = freeRickList.get(j).id;
          j++;
        } else {
          break;
        }
      }
    }
    update newMortyList;
  }

  public static void create200ContactsRickAndMorty() {
    List<Contact> conList = new List<Contact>();
    for (Integer i = 0; i < 200; i++) {
      Integer rand = Integer.valueOf(Math.random() * 2);
      if (rand == 0) {
        conList.add(new Contact(FirstName = 'Rick', LastName = 'Sanchez'));
      } else {
        conList.add(new Contact(FirstName = 'Morty', LastName = 'Smith'));
      }
    }
    System.debug('-------------------------------------------------');
    insert conList;
  }
}
