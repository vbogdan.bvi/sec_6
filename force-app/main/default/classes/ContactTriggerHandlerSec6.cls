/**
 * @author            : Viktor Bogdan
 * @last modified on  : 07-02-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   07-02-2021   Viktor Bogdan   Initial Version
 **/
public with sharing class ContactTriggerHandlerSec6 {
  /*

    3. Создайте триггер на объекте Contact, который при создании
    контакта, будет вытягивать все существующие Аккаунты с
    созданным выше полем(Activity типа Date). Далее высчитывается средняя дата
    между всеми из этих Аккаунтов по данному полю и Контакт
    присваивается к Аккаунту с этой датой или датой самой близкой
    к ней. Если есть несколько Аккаунтов с одинаковым значением
    поля Activity, при подсчете средней даты берется только 1
    Случайный Аккаунт. К Аккаунтам, к которым уже был привязан
    Контакт больше нельзя привязать еще 1.

    4. Если контактов больше чем аккаунтов или аккаунтов нет, то поле
    isAccountSet на Контакте выставляется на false, если же Аккаунт
    был успешно присвоен – true.
  */
  public static void addAccountToContact(List<Contact> newContacts) {
    List<Account> accounts = [
      SELECT Name, Activity__c
      FROM Account
      WHERE Activity__c != NULL AND ID NOT IN (SELECT AccountID FROM Contact)
    ];

    Long averageTimeMS = calcAverageTimeMS(accounts);
    sortAccounts(accounts, averageTimeMS);

    Integer j = 0;
    for (Contact cont : newContacts) {
      if (j < accounts.size()) {
        cont.AccountId = accounts[j].ID;
        cont.isAccountSet__c = true;
        j++;
      } else {
        break;
      }
    }
  }

  /**
   * @ calculation average time in miliseconds
   * @param accounts {List<Account>}
   * @return Long
   **/
  public static Long calcAverageTimeMS(List<Account> accounts) {
    Long averageTime = 0;
    for (Account acct : accounts) {
      // System.debug('test ' + ((DateTime)accounts[0].Activity__c).getTime());
      averageTime = averageTime + ((DateTime) acct.Activity__c).getTime();
    }
    averageTime = (Long) (averageTime / accounts.size());
    return averageTime;
  }

  /**
   * @ sorts the list of accounts, where at the beginning are the accounts with the closest "Activity__c" to the average date
   * @param accounts {List<Account>}
   * @param averageTimeMS Long  - average date in miliseconds
   **/
  public static void sortAccounts(List<Account> accounts, Long averageTimeMS) {
    for (Integer i = 0; i < accounts.size(); i++) {
      Long timeDiff1 = Math.abs(((Datetime) accounts[i].Activity__c).getTime() - averageTimeMS);
      Integer index = i;
      for (Integer j = i + 1; j < accounts.size(); j++) {
        Long timeDiff2 = Math.abs(((Datetime) accounts[j].Activity__c).getTime() - averageTimeMS);
        if (timeDiff2 < timeDiff1) {
          timeDiff1 = timeDiff2;
          index = j;
        }
      }
      if (i != index) {
        Account tmp = accounts[i];
        accounts[i] = accounts[index];
        accounts[index] = tmp;
      }
    }
  }
}
